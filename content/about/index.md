---
title: About
date: 2022-12-14T01:47:05+02:00
draft: false
---

## Who am I?

```shell
whoami
```

I'm a man who decided to become a Software Developer. Currently I am a
student at Wroclaw University of Science and Technology in the field of
**Control Engineering and Robotics**.  During my studies I develop skills
related to embedded systems **(C / C++)** as well as in the field of **Linux**
administration. In spare time I try to develop my scripting skills in **Bash**
or **Python**. Apart from studies and any technical related activities I like
skiing and hiking.

## Why this website even exist?

```shell
nmap -sC -sV -O
```

Initially, this site was based on **Wordpress** and **MySQL**. I wanted to learn
**continuous integration** and **deployment**, so I started building this
website again as static. I was writing the site for a few months but recently
discovered **Hugo** and the site gained momentum. Currently, I am still
developing in **CI/CD**, and I left the site and decided to publish my projects
on it.
