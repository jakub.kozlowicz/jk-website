---
title: Calculator RPN in C
date: 2020-01-24
author: Jakub Kozłowicz
draft: false
---

## Description

This was my last project at Basic Programming course. After [image
processing][2] this program was a little bit relaxing. Main aim was to learn
**dynamic memory allocation** and **stack concept**.

This program was meant to behave like Unix program - `dc`. It use reverse polish
notation which allow to skip parenthesis.

Code for this project are available on my [Gitlab][1].

---

*This project was made for my university course called "Basic Programming"
at Wroclaw University of Science and Technology.*

[1]: https://gitlab.com/jakub.kozlowicz/calculator-rpn
[2]: {{< ref "projects/images-processing/index.md" >}}
