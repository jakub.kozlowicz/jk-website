---
title: Home server lab
date: 2021-06-15
author: Jakub Kozłowicz
draft: false
---

## Description

The main goal of this project was to "create" home router/server. We had to
create three **virtual machines**. One main was the router and other two were
clients on different **subnets**. Our responsibilities were to configure
**DHCP server** with **routing** and **NAT**. Alongside this on main machine
we configured **SSH server**, file server **Samba**, **web server** with
**Wordpress** website and last but not least **DNS server**.

Report from this project can be found [here][report], it is written in Polish.

---

*This project was made for my university course called "RTS - Computer
networks" at Wroclaw University of Science and Technology.*

[report]: assets/home-lab-report.pdf
