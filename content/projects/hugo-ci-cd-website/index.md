---
title: Hugo CI/CD website
date: 2021-07-05
author: Jakub Kozłowicz
draft: false
---

## Description

Due to my love to automation, I thought how can I simplify my life during
updating this website. After finding **Hugo**, I thought it would be a good idea
to integrate the repository on **Gitlab** with the available
**CI/CD** options. This project taught me how **continuos integration** works
and how to properly integrate it with the repository. **Continuos delivery**
uses the **Netlify** for updating and serving website.

Source code of this website with `.gitlab-ci.yaml` are on my personal
[Gitlab](https://gitlab.com/jakub.kozlowicz/jk-website).

---

*This project is my personal own work which i decided to do in my spare time.*
