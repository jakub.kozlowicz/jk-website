---
title: Underwater drone simulation in C++
date: 2020-06-15
author: Jakub Kozłowicz
draft: false
---

{{< figure src="./images/render.png" caption="Application renders" width=100% align="center" >}}

## Description

Main goal of this C++ project was to learn **Object Oriented Programming**.
Learn the main concepts of programming such as **classes**, **inheritance**,
and even **templates**. Additionally, with the help of files from teachers,
we could use **Gnuplot** to display the drone animation. It moves in a special
space where collisions are detected.

All of documentation are prepared in English and done with **Doxygen**.
Moreover whole project is available on my [Gitlab][source_code]

### Usage

This terminal based application has added external availability to display
underwater drone animation through Gnuplot. Application has four options:

- `r` - moving straight ahead for a given distance with a given angle of ascent
or descent,
- `o` - change the orientation of the drone (rotation in the OZ axis),
- `m` - display help menu,
- `k` - quit program.

---

*This project was made for my university course called "Object Oriented
Programming" at Wroclaw University of Science and Technology.*

[source_code]: https://gitlab.com/jakub.kozlowicz/drone-simulation
