---
title: Tic Tac Toe game with AI in C++
date: 2021-06-10
author: Jakub Kozłowicz
draft: false
---

{{< figure src="./images/render.png" caption="Application renders" width=100% align="center" >}}

## Description

The aim of this project was to learn how to work with **external libraries**,
how **graphical interface based applications** works and designing
**artificial intelligence algorithms** to allow playing against virtual
player (computer). For this purpose, the **SFML 2.5** library was chosen.

All of source code and documentation for it are written in English. You can
find this on my [Gitlab][1].

### Usage

Instruction how to build this application is in README on Gitlab repository.

#### Possibilities

Game allow you to choose playing board size and number of signs in order to win.
User pick their sign and according to chosen sign will start or computer will make
first move. Always `X` sign has first move.

#### Limitations

- User can pick playing board size from 3 to 9
- Number of sign in line in order to win cannot be greater than playing board

---

*This project was made for my university course called "Designing algorithms and
methods of artificial intelligence" at Wroclaw University of Science and
Technology.*

[1]: https://gitlab.com/jakub.kozlowicz/tic-tac-toe-game
