---
title: DataLogger operating system
date: 2021-09-20
author: Jakub Kozłowicz
draft: false
---

{{< figure src="./images/device-circuit.png" caption="Device electronic circuit" width=60% align="center" >}}

## Description

After deciding that I would like to pursue my student internship by promotion,
it is time to create an **embedded system** for **industrial automation**. The
goal was to create a minimal **Linux** distribution using the **Yocto Project**.
My distribution is called GolemOS, and it is based on the reference distribution
of **Poky**. During my work I learned a lot about the initialization processes
system which in this case was **systemd**. I also learned the secrets of setting
up a **hotspot**, **web server (nginx)**, or writing websites with **Flask**.
Complementing the whole thing with a lot of **scripts** written in **Python**.

Useful links related to this project:

- [Source code [EN]](https://gitlab.com/kanri-logger/meta-golemos/-/tree/datalogger)
- [Documentation [EN]](http://panamint.kcir.pwr.edu.pl/~jkozlowi/golemos/)

---

*This project was made for my summer internship at [Kanri Soft](https://kanrisoft.eu).*
