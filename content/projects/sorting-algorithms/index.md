---
title: Sorting algorithms in C++
date: 2021-04-17
author: Jakub Kozłowicz
draft: false
---

## Description

Project written in C++ with aim to design **sorting algorithms** and become
more familiar with **external c++ library** for **unit tests** called
**catch2**. We had to implement a few algorithms so I chose **quick sort**,
**insertion sort** and **bubble sort**.

For me this task was not enough so I decided to test this algorithms
simultaneously. I wrote performance tests with **multithreading**.

All of source code with tests written with **catch2** are available on my [Gitlab][1].

### Usage

This is terminal based applications. **CMake** are generating three binaries:

- performance tests,
- quality tests,
- data generator to use on Windows systems.

Results from performance tests are saved under `result` folder and stored in
`csv` file format.

---

*This project was made for my university course called "Designing algorithms and
methods of artificial intelligence" at Wroclaw University of Science and
Technology.*

[1]: https://gitlab.com/jakub.kozlowicz/sorting
