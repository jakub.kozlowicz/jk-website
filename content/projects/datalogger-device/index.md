---
title: DataLogger embedded device
date: 2022-02-02
author: Jakub Kozłowicz
draft: false
---

{{< figure src="./images/render.jpg" caption="Device render" width=40% align="center" >}}

## Description

The aim of this project was to combine two subjects into one project and during
the entire semester create a fully functional device used in **industrial
automation**. The project was carried out in groups of two, and we learned more
about programming **AVR microcontrollers**, things related to data buses
(**SPI**, **I2C**, **UART**) or controlling electronic circuits using **PWM**.
From the electronic aspect, we have learned more about the operation of
**operational amplifiers**, **optocouplers** and **linear voltage stabilizers**.
There were also many types of diodes, such as **Zener diodes**, **TVS diodes**
or **Schottky diodes**.

Bonus part to this project was to create own **custom PCB** and close all parts
in enclosure.

Useful links related to this project:

- [Source code [EN]][code]
- [Electronic schematic [EN]][electronics]
- [Documentation [PL]][documentation]

---

*This project was made for my university courses called "Basics of
microprocessor technology" and "Electronics in Automation" at Wroclaw University
of Science and Technology.*

[code]: https://gitlab.com/industrial-logger/logger-code
[electronics]: assets/datalogger-device-circuit.pdf
[documentation]: assets/datalogger-device-documentation.pdf
