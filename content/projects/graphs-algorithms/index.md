---
title: Dijkstra and Bellman-Ford algorithms in C++
date: 2021-05-10
author: Jakub Kozłowicz
draft: false
---

## Description

Main goal for this project was to write two algorithms which can find the
quickest way to proper vertex. We had to write **Dijkstra** and
**Bellman-Ford** algorithms and compare them to find which one is better
for certain situation. This project is also written with **unit tests** in
**catch2** to ensure all algorithms are correct.

Performance testing was again slow for me like in [sorting algorithms][2]
so I wrote them the same way and implement **multithreading**.

All of source code for this project is available on my [Gitlab][1].

### Usage

This is terminal based application and **CMake** is generating two
applications:

- test graphs,
- efficiency test.

Result files from efficiency test are stored under `result` folder in `csv`
format file.

---

*This project was made for my university course called "Designing algorithms and
methods of artificial intelligence" at Wroclaw University of Science and
Technology.*

[1]: https://gitlab.com/jakub.kozlowicz/graphs
[2]: {{< ref "projects/sorting-algorithms/index.md" >}}
