---
title: Images processing in C
date: 2020-01-04
author: Jakub Kozłowicz
draft: false
---

## Description

For sure I can say that this was the biggest programming project on my first
semester. Main goal was to learn about **structures**, **dynamic memory
allocation** and **pointers**.

Program add filters to both PGM and PPM images. Also allow to convert
PPM image format to PGM.

All of source code for this project are available on my [Gitlab][1].
In README of this repository is whole instruction how to use options
of this program.

---

*This project was made for my university course called "Basic Programming"
at Wroclaw University of Science and Technology.*

[1]: https://gitlab.com/jakub.kozlowicz/images-processing
